# Configuration files and Scripts

This holds configuration files and scripts used in various places.
The files are mainly for Linux or Cywgin/X.

### Environment Setup Instructions
### WSL

* Follow the instructions [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10) to get Ubuntu running. 

* After you are in, update the packages, by running: 

```bash
sudo apt-get update 
```
### ZSH Configuration

* Open the Terminal and run

```bash
sudo apt-get install zsh
```

* We now have to modify the settings to make ZSH our default shell using the chsh command

```bash
sudo chsh -s $(which zsh)
```

* Now install Oh-my-zsh with:

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

* The `zsh` shell should be activated during the Oh-my-zsh install `zsh` but if not close the Terminal and run it again.

* Install PowerLevel10K with:

```bash
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
```

* Copy the contents of the file [zshrc](configurations/zshrc) to `~/.zshrc`

* Edit the `TODO:` blocks in ~/.zshrc as needed to:
    * Set the path to OH-MY-ZSH
    * Create any navigation aliases
    * Set any environment variables required

* Copy the contents of the file [p10k.zsh](configurations/p10k.zsh) to `~/.p10k.zsh`
    * This bypasses the need to run `p10k configure`
#### Extra Plugins

We can install some custom plugins for ZSH and Oh-my-ZSH

* Install the `zsh-autosuggestions` plugin with:

```bash
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
```

### Windows Terminal

* Windows Terminal can be installed from the Windows Store. Once installed we will configure the settings, the distributions available and customize the colors, font and keyboard bindings .

* Install the 4 fonts from the `MesloLGS NF` family from the [fonts/MesloLGSNF](fonts/MesloLGSNF/ ) directory

* Edit the Windows Terminal `settings.json`


### SSH Agent and GPG Signing

We will use an ssh-agent to load SSH keys into terminal sessions so that we can easily login to remote machines.

* Install the Ubuntu package called keychain

```bash
sudo apt-get install keychain
```

* Add this to the **SSH Agent and Keyring** section of the `~/.zshrc` file.
    * Note that you may have to double-check the location of the `keychain` executable.

```bash
# SSH Agent and Keyring
export HOST_NAME=$(hostname)
export SSH_KEY=/home/jenright/.ssh/id_rsa
/usr/bin/keychain ${SSH_KEY} 2> /dev/null
[[ -f ~/.keychain/${HOST_NAME}-sh ]] && source ~/.keychain/${HOST_NAME}-sh
```

* TODO: GPG signing
## Summary of Files in Repo

### Configuration Files
* **config**: ssh config file. Place in the ~/.ssh folder on Linux or Cywgin/X

* **file.Xresources**: Configuration for X Windows. Useful with Cywgin/X. Rename to .Xresources and place in home directory on Linux.

* **gpg-agent.conf**: Agent Options file for GNU Privacy Guard (GnuPG).

* **pk10k.zsh**: Powerlevel10k configuration file for use in Linux or WSL Terminal setup.

* **vimrc**: A vim configuration file that gives a fairly nice look and feel and indentation.

* **zshrc**: Zsh configuration file used in above 

* Under the **windows-terminal/** directory
    * **custom_schemes**: Colour scheme definition for Windows Terminal.
    * **ubuntu_config**: Ubuntu configuration for Windows Terminal.
    * **settings.json**: Example of full Windows Terminal configuration file. For reference only.
### Scripts

* **make_snapshot.sh**: BASH script to make a rotating backup and snapshot of a folder. Useful on Linux or Windows through Cywgin/X. Uses `rsync` and `cp -al`

* **sync_opi_validation_files.sh**: BASH script to copy files from one folder to another. Useful on Linux or Windows through Cywgin/X. Uses `rsync` and `cp -al`

## fonts

* The **fonts/** directory contains 4 MesloLGS NF font files that are used in the Zsh terminal.

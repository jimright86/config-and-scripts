#!/bin/bash
# ----------------------------------------------------------------------
# Copies the latest version of the OPI Validation files to local machine
# Uses rsync
#
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
unset PATH	# avoid accidental use of $PATH

# ---------------- system commands used by this script -------------------------
ECHO=/bin/echo
TOUCH=/bin/touch
TEE=/usr/bin/tee
DATE=/usr/bin/date
BASENAME=/usr/bin/basename
DIRNAME=/usr/bin/dirname

RSYNC=/usr/bin/rsync

# -------------------- variables & file locations ------------------------------

#'//iveraghdata/dcc/it/qa/hospitality/OPI\ Certification/'
REMOTE_DIR='/cygdrive/y/OPI Certification' # network directory to copy
LOCAL_DIR='/cygdrive/c/Users/jenright/data/work/FEXCO/2018/OpenConnect/Hospitality' # local directory where files are copied to

#EXCLUDES=/usr/local/etc/backup_exclude;
#BASEDIR_TO_SAVE=`$BASENAME $DIR_TO_SAVE` #enclose in "" because it has a space
# See https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
SCRIPT_SOURCE="${BASH_SOURCE[0]}"
SCRIPT_BASE="$( $BASENAME "$SCRIPT_SOURCE" )"
SCRIPT_DIR_RELATIVE="$( $DIRNAME "$SCRIPT_SOURCE" )" # relative path
SCRIPT_DIR_FULL="$( cd "$( $DIRNAME "$SCRIPT_SOURCE" )" && pwd )" # full path
echo "Source is ${SCRIPT_SOURCE}. Basename is ${SCRIPT_BASE}. Rdir is ${SCRIPT_DIR_RELATIVE}. Dir is ${SCRIPT_DIR_FULL}"

# See https://stackoverflow.com/questions/13210880/replace-one-substring-for-another-string-in-shell-script
SCRIPT_SOURCE_TRIM=${SCRIPT_BASE/.sh/}
DATE=`$DATE '+%d_%m_%Y-%H-%M'` # save the date
LOGFILE="$SCRIPT_DIR_FULL/logs/${SCRIPT_SOURCE_TRIM}_${DATE}.log"
echo "Log written to $LOGFILE"

# -------------------- Log output to file -------------------------------------
# log to file
exec > >($TEE -a "$LOGFILE")
exec 2>&1

# -------------------- make rotating snapshot ----------------------------------

# Step 1:
# rsync from the $REMOTE_DIR into the $LOCAL_DIR
echo "Starting Step 1, Rsync"

echo $RSYNC	-s -va "$REMOTE_DIR" $LOCAL_DIR/

$RSYNC	-s -va "$REMOTE_DIR" $LOCAL_DIR/
#--delete --exclude 'originalFiles'

echo "Step 1, Complete"

#!/bin/bash
# ----------------------------------------------------------------------
# Rotating backup and snapshot utility
# Uses rsync & cp -al
# ----------------------------------------------------------------------
# Makes a rotating backup-snapshots of $DIR_TO_SAVE whenever called
# ----------------------------------------------------------------------
# References
# * http://www.mikerubel.org/computers/rsync_snapshots/
# ----------------------------------------------------------------------

unset PATH	# avoid accidental use of $PATH

# ---------------- system commands used by this script -------------------------
ECHO=/bin/echo
RM=/bin/rm
MV=/bin/mv
CP=/bin/cp
TOUCH=/bin/touch
TEE=/usr/bin/tee
DATE=/usr/bin/date
BASENAME=/usr/bin/basename
DIRNAME=/usr/bin/dirname

RSYNC=/usr/bin/rsync

# -------------------- variables & file locations ------------------------------

DIR_TO_SAVE="/cygdrive/c/Users/jenright/data" # the name of the directory to backup
SNAPSHOT_DIR="/cygdrive/i/backups" # name of the directory where backups are placed
#EXCLUDES=/usr/local/etc/backup_exclude;
BASEDIR_TO_SAVE=`$BASENAME $DIR_TO_SAVE` #enclose in "" because it has a space

# See https://stackoverflow.com/questions/13210880/replace-one-substring-for-another-string-in-shell-script
DATE=`$DATE '+%d_%m_%Y-%H-%M'` # save the date
LOGFILE=$SNAPSHOT_DIR/logs/backup_${DATE}.log
echo "Log written to $LOGFILE"
# -------------------- Log output to filet -------------------------------------
# log to file
exec > >($TEE -a $LOGFILE)
exec 2>&1

# -------------------- make rotating snapshot ----------------------------------

# Step 1:
# make a hard-link-only (except for dirs) copy of the latest snapshot, if that exists
echo "Starting Step 1, Copy"
if [ -d $SNAPSHOT_DIR/${BASEDIR_TO_SAVE}.0 ] ; then
  $CP -al $SNAPSHOT_DIR/${BASEDIR_TO_SAVE}.0 $SNAPSHOT_DIR/${BASEDIR_TO_SAVE}.${DATE}
fi
echo "Step 1, Complete"

# Step 2:
# rsync from the $DIR_TO_SAVE into the latest snapshot
echo "Starting Step 2, Rsync"
echo $RSYNC								\
	-va --delete --delete-excluded				\
	$DIR_TO_SAVE/ $SNAPSHOT_DIR/${BASEDIR_TO_SAVE}.0
$RSYNC								\
  -va --delete --delete-excluded				\
  $DIR_TO_SAVE/ $SNAPSHOT_DIR/${BASEDIR_TO_SAVE}.0
#	--exclude-from="$EXCLUDES"				\
echo "Step 2, Complete"
